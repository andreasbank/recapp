#include "StdAfx.h"
#include "Week.h"


CalendarControl::Week::Week(void) {
	WeekStart = DateTime::Now;
	Events = gcnew ArrayList();
}

CalendarControl::Week::Week(DateTime ^weekStart) {
	WeekStart = weekStart;
	Events = gcnew ArrayList();
}