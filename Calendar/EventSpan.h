#pragma once

using namespace System;
using namespace System::Drawing;

namespace CalendarControl {

	public ref class EventSpan
	{
	public:
		property String ^EventTitle;
		property DateTime^ StartTime;
		property DateTime^ EndTime;
		EventSpan(void);
		EventSpan(String ^, DateTime^, DateTime^); // StartTime and EndTime
	};
}