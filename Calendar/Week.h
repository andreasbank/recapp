#pragma once
#include "EventSpan.h"

using namespace System;
using namespace System::Collections;
using namespace System::Drawing;

namespace CalendarControl {
	public ref class Week {
	public:
		property DateTime ^WeekStart;
		property Rectangle ^WeekRectangle;
		ArrayList ^Events;
		Week(void);
		Week(DateTime ^);
	};
}
