#include "StdAfx.h"
#include "EventSpan.h"

CalendarControl::EventSpan::EventSpan(void) {
	EventTitle = L"";
	StartTime = DateTime::Now;
	EndTime = DateTime::Now;
}

CalendarControl::EventSpan::EventSpan(String ^EventTitle, DateTime^ StartTime, DateTime^ EndTime) {
	this->EventTitle = EventTitle;
	this->StartTime = StartTime;
	this->EndTime = EndTime;
}
