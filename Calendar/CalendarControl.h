/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	Copyright (C) 2011 by Andreas Bank, andreas.mikael.bank@gmail.com
 *
 *	CalendarControl.h
 *	A simple calendar drawing containing four weeks (columns)  that can
 *	contain events in form of drawn rectangles positioned according
 *	to the contained start and end date/time in the week.
 *
 *	This source code is distributed under the GPL version 3.
 *	bla bla bla
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#pragma once


#include "Week.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
//using namespace System::Collections::Generic;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::Globalization;


namespace CalendarControl {

	/// <summary>
	/// Summary for CalendarControl
	/// </summary>
	public ref class CalendarControl : public System::Windows::Forms::UserControl {
		Week ^Week1, ^Week2, ^Week3, ^Week4;
		array<Week^> ^Weeks;
		array<String^> ^WeekDays;
	public:
		ArrayList^ Events;
		Boolean IsLeapYearBool;

		CalendarControl(void)
		{
			DateTime ^today = DateTime::Now;
			Events = gcnew ArrayList();

			// some events for testing purpouses
			Events->Add(gcnew EventSpan(L"Car1", today, today->Add(TimeSpan(10, 0, 0))));
			Events->Add(gcnew EventSpan(L"Car2", today->AddDays(7), today->AddDays(9)));

			// fill the weekdays array with the appropriate strings
			WeekDays = gcnew array<String^>(7);

			// instanciate the Weeks array with 4 weeks (still null pointers)
			Weeks = gcnew array<Week^>(4);

			// maybe replace with DayOfWeek::Monday->ToString("dddd", "sv-SE"); or something... ???
			WeekDays[0] = L"M�ndag";
			WeekDays[1] = L"Tisdag";
			WeekDays[2] = L"Onsdag";
			WeekDays[3] = L"Torsdag";
			WeekDays[4] = L"Fredag";
			WeekDays[5] = L"L�rdag";
			WeekDays[6] = L"S�ndag";

			// initialise the forms components
			InitializeComponent();

			// create our four weeks
			CreateWeeks(today);

		} // CalendarControl

		CalendarControl(ArrayList^ Events) {
			this->Events = Events;
		} // CalendarControl

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~CalendarControl()
		{
			if (components)
			{
				delete components;
			}
		}

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->SuspendLayout();
			// 
			// CalendarControl
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::Control;
			this->MaximumSize = System::Drawing::Size(600, 300);
			this->MinimumSize = System::Drawing::Size(300, 300);
			this->Name = L"CalendarControl";
			this->Size = System::Drawing::Size(600, 300);
			this->Click += gcnew System::EventHandler(this, &CalendarControl::CalendarControl_Click);
			this->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &CalendarControl::CalendarControl_Paint);
			this->ResumeLayout(false);

		}
#pragma endregion

		/**
		 * CreateWeeks()
		 */
		Void CreateWeeks(DateTime ^today) {

			// calculate today as integer (ex. tueseday = 2)
			short todayAsInt = GetDayAsInteger(today->ToString("dddd", gcnew CultureInfo("sv-SE")));

			// for every week
			for(short i=0; i<4; i++) {

				// create week instance
				Weeks[i] = gcnew Week(today->AddDays(7 * i - todayAsInt));

				// create the weeks rectangular representation positions on the calendar
				Weeks[i]->WeekRectangle = gcnew Rectangle(10 + 145 * i, 10, 10 + ((this->Width - 20) / 4) * (i + 1), 290);

				// get enumerator
				IEnumerator^ enumerator = Events->GetEnumerator();

				// browse through all the Events
				while(enumerator->MoveNext()) {

					// point to one
					EventSpan^ MyEvent = (EventSpan^) enumerator->Current;

					// calculate today as integer (ex. tueseday = 2)
					short eventStartAsInt = GetDayAsInteger(MyEvent->StartTime->ToString("dddd", gcnew CultureInfo("sv-SE")));
					
					short eventEndAsInt = GetDayAsInteger(MyEvent->EndTime->ToString("dddd", gcnew CultureInfo("sv-SE")));

					// check if it falls into week 1
					if(MyEvent->StartTime->Day - eventStartAsInt ==  Weeks[i]->WeekStart->Day || MyEvent->EndTime->Day - eventEndAsInt ==  Weeks[i]->WeekStart->Day) {
				
						// add it to week [i] events array
						Weeks[i]->Events->Add(MyEvent);
					}
				
				} // while
			} // for
		} // CreateWeeks

		/**
		 * CalendarControl_Click()
		 */
		Void CalendarControl_Click(System::Object^  sender, System::EventArgs^  e) {
			// TODO: check for click-collision
			//MessageBox::Show(L"click!\n");
		} // CalendarControl_Click

		/**
		 * CalendarControl_Paint()
		 */
		Void CalendarControl_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {

			// get a pointer to the form graphics
			Graphics^ g = e->Graphics;

			// clear it beforw we draw
			g->Clear(SystemColors::Control);

			// create a brush for drawing the rows background
			SolidBrush^ myBrush = gcnew SolidBrush(Color::White);

			// text string, used for all text drawing
			String^ myString;

			// create a font object, also for drawing text
			System::Drawing::Font^ myFont = gcnew System::Drawing::Font("Arial", 8.0f);

			// get the first day of the week as a date number [1-31]
			short firstDayOfWeek = GetFirstDayOfWeekAsDate();

			// draw the raws in 2 different colors
			for(short i=0; i<7; i++) {

				// fill one rows background
				g->FillRectangle(myBrush, 10, 10+(this->Height-20)/7*i, this->Width - 20, 40);

				// switch background color
				if(myBrush->Color.Equals(Color::White)) myBrush->Color = Color::Gray;
				else myBrush->Color = Color::White;

			} // for

			// create a pen for drawing the borders
			Pen^ pen = gcnew Pen(Color::Black, 1);

			// draw the month border
			g->DrawRectangle(pen, 10, 10, this->Width-20, this->Height-20);

			// change brush color to use for drawing text
			myBrush->Color = Color::Black;

			// create a new brush for drawing event rectangles (event background)
			SolidBrush ^eventBrush = gcnew SolidBrush(Color::SkyBlue);

			for(short i=1; i<5; i++) {

				// draw all weeks (the right border) in a month
				g->DrawLine(pen,10+145*i,10,10+((this->Width-20)/4)*i,this->Height-10);

				// create a string indicating the week number (ex. "Vecka 1")
				myString = L"Vecka " + GetWeekNumber(i-1);

				// draw the string above
				g->DrawString(myString, myFont, myBrush, PointF((float)(((this->Width-20)/8)-12+((this->Width-20)/4)*i-(this->Width-20)/4), 12.0f));
				// above PointF: (calendar padding) 10 + (text padding) 20.0f + (week number) 145*i - (start at zero) 145

				// loop through the four weeks
				for(short j=0; j<7; j++) {

					// figure out the date of the current day and convert it to a string
					myString = (firstDayOfWeek++).ToString();

					// if the first day gets pass this months days, make it 1
					if(firstDayOfWeek > GetMonthDays()) firstDayOfWeek = 1;

					// draw the date of the current day
					g->DrawString(myString, myFont, myBrush, PointF(12.0f+((this->Width-20)/4)*i-(this->Width-20)/4, ((float)30+40*j)));

				} // for

				// draw the events (calculated earlier) that fall in this week (Weeks[i]->Events)
				IEnumerator ^myEnum = Weeks[i-1]->Events->GetEnumerator();
				while(myEnum->MoveNext()) {

					// point to the current event
					EventSpan ^myEvent = (EventSpan ^) myEnum->Current;

					// calculate a rectangle to draw
					Rectangle ^eRect = GetEventCoordinates(myEvent, i-1);

					// draw the damn thing!
					if(eRect->Height < 10) eRect->Height = 10;
					g->FillRectangle(eventBrush, eRect->X, eRect->Y, eRect->Width, eRect->Height);
					g->DrawRectangle(pen, eRect->X, eRect->Y, eRect->Width, eRect->Height);
					g->DrawString(myEvent->EventTitle, myFont, myBrush, PointF(10.0f + eRect->X, (eRect->Height<40?eRect->Y+eRect->Height/2-6:10.0f + eRect->Y)));
					if(eRect->Height >= 40) {
						g->DrawString(myEvent->StartTime->ToString(), myFont, myBrush, PointF(10.0f + eRect->X, 22.0f + eRect->Y));
						g->DrawString(myEvent->EndTime->ToString(), myFont, myBrush, PointF(10.0f + eRect->X, 34.0f + eRect->Y));
					}
					if(eRect->Height >= 50)
						g->DrawString("some more info", myFont, myBrush, PointF(10.0f + eRect->X, 46.0f + eRect->Y));

				}

			} // for
		} // CalendarControl_Paint

		/**
		 * GetEventCoordinates()
		 */
		Rectangle ^GetEventCoordinates(EventSpan ^myEvent, short weekNumber) {

			// X
			short X = 40 + 145 * weekNumber;

			// test
			short aaa = GetDayAsInteger(myEvent->StartTime->ToString("dddd", gcnew CultureInfo("sv-SE")));

			// Y
//			short Y = 10 + (this->Height - 20) * GetDayAsInteger(myEvent->StartTime->ToString("dddd", gcnew CultureInfo("sv-SE"))) / 7;
			short Y = 10 + (this->Height - 20) * (GetDayAsInteger(myEvent->StartTime->ToString("dddd", gcnew CultureInfo("sv-SE"))) * 24 + myEvent->StartTime->Hour) / 168;

			// Width
			short Width = (this->Width - 140) / 4;

			// Height
			//short Height = 10 + (this->Height - 20) * GetDayAsInteger(myEvent->EndTime->ToString("dddd", gcnew CultureInfo("sv-SE"))) / 7 - Y;
			short Height = 10 + (this->Height - 20) * (GetDayAsInteger(myEvent->EndTime->ToString("dddd", gcnew CultureInfo("sv-SE"))) * 24 + myEvent->EndTime->Hour) / 168 - Y;

			// if the event is in one week return a normal rectangle
			if(GetWeekNumber(myEvent->StartTime) == GetWeekNumber(myEvent->EndTime)) {
				return gcnew Rectangle(X, Y, Width, Height);
			} // if

			// else return a rectangle that ends at the end of the week
			return gcnew Rectangle(X, Y, Width, Weeks[weekNumber]->WeekRectangle->Height - Y);
		}

		/**
		 *	GetFirstDayOfWeekAsDate()
		 *	Calculates the date of the weeks first day.
		 */
		Int32 GetFirstDayOfWeekAsDate() {
			DateTime^ today = DateTime::Now;

			// calculate the day
			short firstDayOfWeekInt = today->Day - GetDayAsInteger(today->ToString("dddd", gcnew CultureInfo("sv-SE")));

			// if 0 or negative, it's previous month
			if(firstDayOfWeekInt < 1) {
				return GetPreviousMonthDays() - firstDayOfWeekInt;
			} // if
			
			// else return the value
			return firstDayOfWeekInt;

		} // GetFirstDayOfWeekAsDate

		/**
		 * GetDayAsInteger()
		 */
		Int32 GetDayAsInteger(String^ DayString) {
			short i = 0;

			// check if the string matches and return
			for(; i<7; i++)
				if(String::Equals(WeekDays[i], DayString, StringComparison::CurrentCultureIgnoreCase)) return i;

			// string did not match any day, error
			throw gcnew Exception("Invalid day string!");

		} // GetDayAsInteger

		/**
		 * GetMonthDays()
		 */
		Int32 GetMonthDays(DateTime ^monthDate) {

			// return the number of days in the month
			return DateTime::DaysInMonth(monthDate->Year, monthDate->Month);

		} // GetMonthDays

		/**
		 * GetMonthDays()
		 */
		Int32 GetMonthDays() {

			// return the number of days in the month
			return GetMonthDays(DateTime::Now);

		} // GetMonthDays

		/**
		 * GetPreviousMonthDays()
		 */
		Int32 GetPreviousMonthDays() {

			// get current date
			DateTime ^monthDate = DateTime::Now;

			// go back one month
			monthDate->AddMonths(-1);

			// return the number of days in the month
			return GetMonthDays(monthDate);

		} // GetPreviousMonthDays

		/**
		 * IsLeapYear() - my thanks go to Wikipedia.org
		 */
		Boolean IsLeapYear() {
			DateTime ^today = DateTime::Now;
			if(today->Year % 400 == 0)
				return true;
			else if(today->Year % 100 == 0)
				return false;
			else if(today->Year % 4 == 0)
				return true;
			return false;
		} // IsLeapYear

		/**
		 * GetWeekNumber() - yet again, Wikipedia.org
		 */
		Int32 GetWeekNumber(DateTime ^date) {
			array<Int32> ^monthSums = { 0,31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334 };
			if(IsLeapYearBool) {
				for(short i=2; i<12; i++) {
					monthSums[i]++;
				} // for
			} // if
			return (int) Math::Floor((date->Day + monthSums[date->Month - 1] - GetDayAsInteger(date->ToString("dddd", gcnew CultureInfo("sv-SE"))) + 10) / 7);
		} // GetWeekNumber

		/**
		 * GetWeekNumber()
		 * Returns the week number of the future
		 * week providedin weeksFromNow
		 */
		Int32 GetWeekNumber(int weeksFromToday) {
			DateTime ^date = DateTime::Now.AddDays(7*weeksFromToday);
			return GetWeekNumber(date);
		} // GetWeekNumber
	};
}
