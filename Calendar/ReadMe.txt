Copyright (C) 2011 by Andreas Bank, andreas.mikael.bank@gmail.com

CalendarControl.h
A simple calendar drawing containing four weeks (columns)  that can
contain events in form of drawn rectangles positioned according
to the contained start and end date/time in the week.