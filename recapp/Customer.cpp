#include "StdAfx.h"
#include "Customer.h"

Customer::Customer() {
		this->ID = 0;
		this->Name = "";
		this->Aftername = "";
		this->Pnr = "";
		this->AddressStreet = "";
		this->AddressNumber = "";
		this->AddressPostcode = "";
		this->AddressCity = "";
		this->AddressCountry = "";
		this->Note = "";
}
Customer::Customer(	int ID,
					DateTime^ CreatedDate,
					String^ Name,
					String^ Aftername,
					String^ Pnr,
					String^ AddressStreet,
					String^ AddressNumber,
					String^ AddressPostcode,
					String^ AddressCity,
					String^ AddressCountry,
					String^ Note) {
		this->ID = ID;
		this->CreatedDate = CreatedDate;
		this->Name = Name;
		this->Aftername = Aftername;
		this->Pnr = Pnr;
		this->AddressStreet = AddressStreet;
		this->AddressNumber = AddressNumber;
		this->AddressPostcode = AddressPostcode;
		this->AddressCity = AddressCity;
		this->AddressCountry = AddressCountry;
		this->Note = Note;
}