#include "StdAfx.h"
#include "User.h"

User::User(	int ID,
			DateTime^ CreatedDate,
			String^ Name,
			String^ Aftername,
			String^ Pnr,
			String^ AddressStreet,
			String^ AddressNumber,
			String^ AddressPostcode,
			String^ AddressCity,
			String^ AddressCountry,
			String^ Username,
			DateTime^ LastLogin) {
		this->ID = ID;
		this->CreatedDate = CreatedDate;
		this->Name = Name;
		this->Aftername = Aftername;
		this->Pnr = Pnr;
		this->AddressStreet = AddressStreet;
		this->AddressNumber = AddressNumber;
		this->AddressPostcode = AddressPostcode;
		this->AddressCity = AddressCity;
		this->AddressCountry = AddressCountry;
		this->Username = Username;
		this->LastLogin = LastLogin;
}
