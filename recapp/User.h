#pragma once

#include "ContactInfo.h"

ref class User : public ContactInfo {
public:
	property String^ Username;
	property DateTime^ LastLogin;
	User(int, DateTime^, String^, String^, String^, String^, String^, String^, String^, String^, String^, DateTime^);
};

