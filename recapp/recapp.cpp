// recapp.cpp : main project file.

#include "stdafx.h"
#include "LoginForm.h"
#include "MainForm.h"

using namespace recapp;

[STAThreadAttribute]
int main(array<System::String ^> ^args)
{
	// Enabling Windows XP visual effects before any controls are created
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false); 

	// create the login window and shoe it
	/*LoginForm^ loginForm = gcnew LoginForm();
	loginForm->ShowDialog();
	if(!loginForm->LoggedIn) return 0;
	loginForm->Close();*/

	// Create the main window and run it
	Application::Run(gcnew MainForm());
	return 0;
}
