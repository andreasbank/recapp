#pragma once
#include "Controller.h"

namespace recapp {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MainForm
	/// </summary>
	public ref class MainForm : public System::Windows::Forms::Form
	{
		Controller^ controller;
	public:
		MainForm(void)
		{
			controller = gcnew Controller();
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MainForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  btnExit;




	private: System::Windows::Forms::SplitContainer^  splitContainer1;
	private: System::Windows::Forms::TreeView^  treeView1;
	private: System::Windows::Forms::ToolStrip^  toolStrip1;
	private: System::Windows::Forms::ToolStripButton^  tstbtnOne;
	private: System::Windows::Forms::StatusStrip^  statusStrip1;
	private: System::Windows::Forms::ToolStripStatusLabel^  tsslblOne;
	private: ::CalendarControl::CalendarControl^  calendarControl1;
	private: ::CalendarControl::CalendarControl^  calendarControl2;

	protected: 

	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MainForm::typeid));
			this->btnExit = (gcnew System::Windows::Forms::Button());
			this->splitContainer1 = (gcnew System::Windows::Forms::SplitContainer());
			this->treeView1 = (gcnew System::Windows::Forms::TreeView());
			this->toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
			this->tstbtnOne = (gcnew System::Windows::Forms::ToolStripButton());
			this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
			this->tsslblOne = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->calendarControl1 = (gcnew ::CalendarControl::CalendarControl());
			this->calendarControl2 = (gcnew ::CalendarControl::CalendarControl());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->splitContainer1))->BeginInit();
			this->splitContainer1->Panel1->SuspendLayout();
			this->splitContainer1->Panel2->SuspendLayout();
			this->splitContainer1->SuspendLayout();
			this->toolStrip1->SuspendLayout();
			this->statusStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// btnExit
			// 
			this->btnExit->Location = System::Drawing::Point(916, 498);
			this->btnExit->Name = L"btnExit";
			this->btnExit->Size = System::Drawing::Size(117, 62);
			this->btnExit->TabIndex = 0;
			this->btnExit->Text = L"Avsluta";
			this->btnExit->UseVisualStyleBackColor = true;
			this->btnExit->Click += gcnew System::EventHandler(this, &MainForm::btnExit_Click);
			// 
			// splitContainer1
			// 
			this->splitContainer1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->splitContainer1->FixedPanel = System::Windows::Forms::FixedPanel::Panel1;
			this->splitContainer1->IsSplitterFixed = true;
			this->splitContainer1->Location = System::Drawing::Point(0, 25);
			this->splitContainer1->Name = L"splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this->splitContainer1->Panel1->Controls->Add(this->treeView1);
			// 
			// splitContainer1.Panel2
			// 
			this->splitContainer1->Panel2->Controls->Add(this->calendarControl2);
			this->splitContainer1->Panel2->Controls->Add(this->btnExit);
			this->splitContainer1->Size = System::Drawing::Size(1241, 563);
			this->splitContainer1->SplitterDistance = 192;
			this->splitContainer1->TabIndex = 5;
			// 
			// treeView1
			// 
			this->treeView1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->treeView1->Location = System::Drawing::Point(0, 0);
			this->treeView1->Name = L"treeView1";
			this->treeView1->Size = System::Drawing::Size(192, 563);
			this->treeView1->TabIndex = 0;
			// 
			// toolStrip1
			// 
			this->toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->tstbtnOne});
			this->toolStrip1->Location = System::Drawing::Point(0, 0);
			this->toolStrip1->Name = L"toolStrip1";
			this->toolStrip1->Size = System::Drawing::Size(1241, 25);
			this->toolStrip1->TabIndex = 6;
			this->toolStrip1->Text = L"toolStrip1";
			// 
			// tstbtnOne
			// 
			this->tstbtnOne->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->tstbtnOne->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"tstbtnOne.Image")));
			this->tstbtnOne->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->tstbtnOne->Name = L"tstbtnOne";
			this->tstbtnOne->Size = System::Drawing::Size(23, 22);
			this->tstbtnOne->Text = L"toolStripButton1";
			// 
			// statusStrip1
			// 
			this->statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->tsslblOne});
			this->statusStrip1->Location = System::Drawing::Point(0, 588);
			this->statusStrip1->Name = L"statusStrip1";
			this->statusStrip1->Size = System::Drawing::Size(1241, 22);
			this->statusStrip1->TabIndex = 7;
			this->statusStrip1->Text = L"statusStrip1";
			// 
			// tsslblOne
			// 
			this->tsslblOne->Name = L"tsslblOne";
			this->tsslblOne->Size = System::Drawing::Size(65, 17);
			this->tsslblOne->Text = L"Laddar fil...";
			// 
			// calendarControl1
			// 
			this->calendarControl1->BackColor = System::Drawing::SystemColors::Control;
			this->calendarControl1->Location = System::Drawing::Point(0, 0);
			this->calendarControl1->MaximumSize = System::Drawing::Size(600, 300);
			this->calendarControl1->MinimumSize = System::Drawing::Size(300, 300);
			this->calendarControl1->Name = L"calendarControl1";
			this->calendarControl1->Size = System::Drawing::Size(600, 300);
			this->calendarControl1->TabIndex = 0;
			// 
			// calendarControl2
			// 
			this->calendarControl2->BackColor = System::Drawing::SystemColors::Control;
			this->calendarControl2->Location = System::Drawing::Point(3, 3);
			this->calendarControl2->MaximumSize = System::Drawing::Size(600, 300);
			this->calendarControl2->MinimumSize = System::Drawing::Size(300, 300);
			this->calendarControl2->Name = L"calendarControl2";
			this->calendarControl2->Size = System::Drawing::Size(600, 300);
			this->calendarControl2->TabIndex = 1;
			// 
			// MainForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1241, 610);
			this->Controls->Add(this->splitContainer1);
			this->Controls->Add(this->statusStrip1);
			this->Controls->Add(this->toolStrip1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
			this->MaximizeBox = false;
			this->Name = L"MainForm";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"recApp v0.1";
			this->WindowState = System::Windows::Forms::FormWindowState::Maximized;
			this->splitContainer1->Panel1->ResumeLayout(false);
			this->splitContainer1->Panel2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->splitContainer1))->EndInit();
			this->splitContainer1->ResumeLayout(false);
			this->toolStrip1->ResumeLayout(false);
			this->toolStrip1->PerformLayout();
			this->statusStrip1->ResumeLayout(false);
			this->statusStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
		private: System::Void btnExit_Click(System::Object^  sender, System::EventArgs^  e) {
			Application::Exit();
		}
	};
}
