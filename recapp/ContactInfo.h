#pragma once

using namespace System;

ref class ContactInfo {
public:
	property int ID;
	property DateTime^ CreatedDate;
	property String^ Name;
	property String^ Aftername;
	property String^ Pnr;
	property String^ AddressStreet;
	property String^ AddressNumber;
	property String^ AddressPostcode;
	property String^ AddressCity;
	property String^ AddressCountry;
};

